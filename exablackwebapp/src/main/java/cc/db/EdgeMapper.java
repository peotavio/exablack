package cc.db;

import java.util.List;
import org.apache.ibatis.annotations.*;

public interface EdgeMapper {
	final String SELECTALL = "SELECT * FROM EDGE WHERE 1=1 ";
    final String INSERT = "INSERT INTO EDGE (SOURCE,TARGET,COST) VALUES (#{source},#{target},#{cost})";

    @Results(value = {
            @Result(property = "source", column = "SOURCE"),
            @Result(property = "target", column = "TARGET"),
            @Result(property = "cost", column = "COST")
    })
    @Select(SELECTALL)
    List<Edge> selectAll();

    @Select(SELECTALL + "and source=#{source}")
    Edge selectBySource(Integer id);

    @Insert(INSERT)
//    @Options(useGeneratedKeys = true, keyProperty = "id") //generate id
    void insert(Edge Edge);
}
