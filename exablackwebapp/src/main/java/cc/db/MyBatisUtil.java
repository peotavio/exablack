package cc.db;

import javax.sql.DataSource;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.h2.jdbcx.JdbcDataSource;

public class MyBatisUtil {
	private static SqlSessionFactory sqlSessionFactory;

    private static SqlSessionFactory openSession() {
        Configuration configuration = getConfiguration(getJndiDatasource());
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
        return sqlSessionFactory;
	}

	private static DataSource getJndiDatasource() {
		JdbcDataSource ds = new JdbcDataSource();
		ds.setUrl("jdbc:h2:./target/db1");
		ds.setUser("user");
		ds.setPassword("pass");
		DataSource dataSource = ds;
		return dataSource;
	}

	private static Configuration getConfiguration(DataSource dataSource) {
		TransactionFactory transactionFactory = new JdbcTransactionFactory();
		Environment environment = new Environment("development", transactionFactory, dataSource);
		Configuration configuration = new Configuration(environment);
		configuration.addMapper(EdgeMapper.class);
		return configuration;
	}

	public static SqlSessionFactory getSqlSessionFactory() {
//        return sqlSessionFactory!=null?sqlSessionFactory:openSession();
		return openSession();
	}
}
