package cc;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import cc.db.Edge;
import cc.db.EdgeMapper;
import cc.db.MyBatisUtil;
import spark.Request;
import spark.Response;

public class EdgeController {

	private EdgeMapper edgeMapper;

	public List<Edge> selectAll(Request req, Response res) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		List<Edge> list = new ArrayList<>();
		try{
			EdgeMapper edgeMapper = sqlSession.getMapper(EdgeMapper.class);
			list = edgeMapper.selectAll();
			System.out.println(list.isEmpty());
			for(Edge it : list){
				System.out.println(it);
			}
			return list;
		}finally{
			sqlSession.close();
		}
	}
}
