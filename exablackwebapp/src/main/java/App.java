import static spark.Spark.*;
//import static cc.JsonUtil.*;

import cc.EdgeController;

import static cc.JsonUtil.*;

public class App {

	public static void main(String[] args) {
		get("/edge", (req, res) -> new EdgeController().selectAll(req, res), json());
	}

}
