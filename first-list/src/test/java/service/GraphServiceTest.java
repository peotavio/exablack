package service;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import domain.Edge;

public class GraphServiceTest {
	private GraphService graphService;

	@Before
	public void setUp() {
		graphService = new GraphService();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArgumentException() {
		String input = "A5,BC9";
		Edge edge = new Edge("ABC", "B", 9);
	}
	
	@Test
	public void testReadGraph() {
		String graph = "AB5,BC4,CD8,DC8,DE6,AD5,CE2,EB3,AE7";
		graphService.readGraph(graph);
	}
	
	@Test
	public void testDistanceRoute() {
		String graph = "AB5,BC4,CD8,DC8,DE6,AD5,CE2,EB3,AE7";
		Set<Edge> adj = graphService.readGraph(graph);
		assertEquals(new Integer(9), graphService.distanceRoute("A-B-C", adj));
		assertEquals(new Integer(5), graphService.distanceRoute("A-D", adj));
		assertEquals(new Integer(13), graphService.distanceRoute("A-D-C", adj));
		assertEquals(new Integer(22), graphService.distanceRoute("A-E-B-C-D", adj));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNoSuchRoute(){
		//INPUTS
		String graph = "AB5,BC4,CD8,DC8,DE6,AD5,CE2,EB3,AE7";
		String route = "A-E-D";
		//END INPUTS
		Set<Edge> adj = graphService.readGraph(graph);
		assertEquals(new Integer(0), graphService.distanceRoute(route, adj));
	}
	
	@Test
	public void testTotalRoutesWithMaxTrips() {
		//INPUTS
		String graph = "AB5,BC4,CD8,DC8,DE6,AD5,CE2,EB3,AE7";
		String source = "C";
		String target = "C";
		int maxTrips = 3;
		//END INPUT
		Set<Edge> adj = graphService.readGraph(graph);
		assertEquals(2, graphService.getTotalRoutesMaxTrips(source, target, maxTrips, adj));
	}
	
	@Test
	public void testTotalRoutesExactlyTrips() {
		//INPUTS
		String graph = "AB5,BC4,CD8,DC8,DE6,AD5,CE2,EB3,AE7";
		String source = "A";
		String target = "C";
		int maxTrips = 4;
		//END INPUT
		Set<Edge> adj = graphService.readGraph(graph);
		assertEquals(3, graphService.getTotalRoutesExactlyTrips(source, target, maxTrips, adj));
	}
	
	@Test
	public void testShortestPathGreedyImpl() {
		//INPUTS
		String graph = "AB5,BC4,CD8,DC8,DE6,AD5,CE2,EB3,AE7";
		String source1 = "A";
		String target1 = "C";
		String source2 = "B";
		String target2 = "B";
		//END INPUT
		Set<Edge> adj = graphService.readGraph(graph);
		assertEquals(9, graphService.getShortestParthGreedyImpl(source1, target1, adj));
		assertEquals(9, graphService.getShortestParthGreedyImpl(source2, target2, adj));
	}
	
	@Test
	public void testPathGreedyImplLessThan() {
		//INPUTS
		String graph = "AB5,BC4,CD8,DC8,DE6,AD5,CE2,EB3,AE7";
		String source = "C";
		String target = "C";
		int lessThan = 30;
		//END INPUT
		Set<Edge> adj = graphService.readGraph(graph);
		assertEquals(7, graphService.totalPathGreedyImplLessThan(source, target, lessThan, adj));
	}
	
	
}
