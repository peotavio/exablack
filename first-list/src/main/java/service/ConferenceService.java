package service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import domain.Conference;
import domain.PeriodOfTime;
import domain.Session;
import domain.Talk;
import domain.Track;

public class ConferenceService {

	public void generateConference(List<Talk> talks) {
		Conference conference = new Conference();
		List<Track> tracks = new ArrayList<>();
		List<Session> session = new ArrayList<>();
		List<Talk> morningTalks = new ArrayList<>();
		List<Talk> afternoonTalks = new ArrayList<>();
		int morningPeriod = (PeriodOfTime.MORNING.getEnd() - PeriodOfTime.MORNING.getStart()) * 60;
		int afternoonPeriod = (PeriodOfTime.AFTERNOON.getEnd() - PeriodOfTime.AFTERNOON.getStart()) * 60;
		for (int i = 1; !talks.isEmpty(); i++) {
			System.out.println("Track #" + i);
			System.out.println("MORNING");
			morningTalks = simulatedAnnealing(morningPeriod, talks);
			System.out.println("SOMA: " + morningTalks.stream().mapToInt(Talk::getLength).sum());
			morningTalks.forEach(System.out::println);
			talks.removeAll(morningTalks);

			System.out.println("AFTERNOON");
			afternoonTalks = simulatedAnnealing(afternoonPeriod, talks);
			System.out.println("SOMA: " + afternoonTalks.stream().mapToInt(Talk::getLength).sum());
			afternoonTalks.forEach(System.out::println);
			talks.removeAll(afternoonTalks);
		}
	}

	private List<Talk> simulatedAnnealing(int totalMaxPeriod, List<Talk> talks) {
		List<Talk> resultTalks = new ArrayList<>();
		if (!talks.isEmpty()) {
			for (int i = 0; i < 10; i++) {
				while (!talks.isEmpty() && resultTalks.stream().mapToInt(Talk::getLength).sum() <= totalMaxPeriod) {
					Talk talk = talks.stream().findAny().get();
					talks.remove(talk);
					resultTalks.add(talk);
				}
				while (!talks.isEmpty() && resultTalks.stream().mapToInt(Talk::getLength).sum() >= totalMaxPeriod) {
					Talk talk = resultTalks.stream().findAny().get();
					resultTalks.remove(talk);
					talks.add(talk);
				}
			}
		}
		return resultTalks;
	}

	private List<Talk> generateWeights(List<Talk> talks, PeriodOfTime periodOfTime) {
		List<Talk> result = new ArrayList<>();
		talks.forEach(s -> {
			Talk talk = new Talk(s.getName(), s.getLength());
			double calc = (double) s.getLength() / ((periodOfTime.getEnd() - periodOfTime.getStart()) * 60);
			talk.setWeight(calc);
			result.add(talk);
		});
		return result;
	}

}
