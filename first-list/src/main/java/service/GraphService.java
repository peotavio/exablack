package service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import domain.Edge;

public class GraphService {

	public Set<Edge> readGraph(String input) {
		String[] graph = input.split(",");
		Set<Edge> adj = new HashSet<Edge>();
		for (String it : graph) {
			String source = it.substring(0, 1);
			String target = it.substring(1, 2);
			Integer cost = new Integer(it.substring(2));
			Edge edge = new Edge(source, target, cost);
			adj.add(edge);
		}
		return adj;
	}

	public Integer distanceRoute(String input, Set<Edge> adj) {
		String[] graph = input.split("-");
		Set<Edge> tmp = new HashSet<Edge>();
		for (int i = 0; i < graph.length - 1;) {
			Edge edge = new Edge(graph[i], graph[++i], 0);
			tmp.add(edge);
		}
		Set<Edge> result = new HashSet<Edge>();
		tmp.stream().forEach(it -> {
			if (validateRoute(it, adj)) {
				result.addAll(adj.stream().filter(s -> s.equals(it)).collect(Collectors.toSet()));
			}
		});
		return result.stream().mapToInt(Edge::getCost).sum();
	}
	
	public Set<Edge> filterEdgesByTarget(String target, Set<Edge> adj){
		return adj.stream()
				.filter(s -> (s.getTarget().equals(target)))
				.collect(Collectors.toSet());		
	}
	
	public Set<Edge> filterEdgesBySource(String source, Set<Edge> adj){
		return adj.stream()
				.filter(s -> (s.getSource().equals(source)))
				.collect(Collectors.toSet());		
	}
	
	public List<Set<Edge>> getRoutesMaxTrips(String source, String target, int maxTrips, Set<Edge> adj){
		List<Set<Edge>> routes = new ArrayList<>();
		routes.add(0,this.filterEdgesByTarget(target, adj));
		for(int i = 0; i < maxTrips-1; i++){
			Set<Edge> tmp = new HashSet<>();
			routes.get(0).stream().forEach(s->{
				tmp.addAll(this.filterEdgesByTarget(s.getSource(), adj));
			});
			routes.add(0,tmp);
		}
		return routes;
	}
	
	public long getTotalRoutesMaxTrips(String source, String target, int maxTrips, Set<Edge> adj){
		List<Set<Edge>> routes = this.getRoutesMaxTrips(source, target, maxTrips, adj);
		long totalRoutes = 0l;
		for(Set<Edge> it : routes){
			Set<Edge> tmp = new HashSet<>();
			tmp = this.filterEdgesBySource(source, it);
			totalRoutes+=tmp.stream().count();
		}
		return totalRoutes;
	}
	
	public long getTotalRoutesExactlyTrips(String source, String target, int maxTrips, Set<Edge> adj){
		List<Set<Edge>> routes = this.getRoutesMaxTrips(source, target, maxTrips+1, adj);
		return routes.get(0).stream().filter(s->s.getSource().equals(source)).count();
	}
	
	public int getShortestParthGreedyImpl(String source, String target, Set<Edge> adj) {
		int maxIt = 99;
		int count = 0;
		int total = 0;
		Set<Edge> routes = this.filterEdgesByTarget(target, adj);
		routes.forEach(System.out::println);
		System.out.println("\n\n");
		while(count < maxIt){
			if(routes.stream().filter(s->s.getSource().equals(source)).count() > 0){
				total+= routes.stream().filter(s->s.getSource().equals(source)).findFirst().get().getCost();
				break;
			}
			int minValue = routes.stream().map(Edge::getCost).reduce(Integer::min).get();
			Edge minRoute = routes.stream().filter(s->s.getCost().equals(minValue)).findFirst().orElse(null);
			if(minRoute.getSource().equals(source)){
				return total;
			}
			routes = this.filterEdgesByTarget(minRoute.getSource(), adj);
			count++;
			total += minValue;
		}
		return total;
	}
	
	public int totalPathGreedyImplLessThan(String source, String target, int lessThan, Set<Edge> adj) {
		int totalRoutes = 0;
		List<Set<Edge>> listRoutes = new ArrayList<>();
		listRoutes.add(0,this.filterEdgesByTarget(target, adj));
//		System.out.println("FIRST");
//		listRoutes.get(0).forEach(System.out::println);
		while(listRoutes.get(0).stream().filter(s->s.getCost() <= lessThan).count() > 0){
			Set<Edge> routes = listRoutes.get(0);			
			Set<Edge> tmp = new HashSet<>();
			for(Edge it : routes){
				if(it.getSource().equals(source)){
					totalRoutes+=1;
				}
				if(it.getCost() <= lessThan){
					Set<Edge> newRoutes = this.filterEdgesByTarget(it.getSource(), adj);
					newRoutes.stream().forEach(s->{
						tmp.add(new Edge(s.getSource(), s.getTarget(), s.getCost()+it.getCost()));
					});
				}
			}
			listRoutes.add(0, tmp.stream().filter(s->s.getCost() <= 30).collect(Collectors.toSet()));
//			System.out.println("\n\n");
//			for(Set<Edge> it : listRoutes){
//				System.out.println(it);
//			}
		}
		
		return totalRoutes;
	}
	
	public int getCost(String source, String target, Set<Edge> adj){
		Set<Edge> routes = this.filterEdgesByTarget(target, adj); 
		return routes.stream().filter(s->s.getSource().equals(source)).mapToInt(Edge::getCost).findFirst().orElse(0);
	}
	
	private boolean validateRoute(Edge edge, Set<Edge> adj) {
		if (adj.stream().filter(s -> s.equals(edge)).findAny().orElse(null) == null) {
			throw new IllegalArgumentException("NO SUCH ROUTE");
		}
		return true;
	}
}
