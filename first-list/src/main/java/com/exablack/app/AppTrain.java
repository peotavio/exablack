package com.exablack.app;

import java.util.Scanner;

import service.GraphService;

public class AppTrain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("input the graph: ");
		String input = scanner.nextLine();
		GraphService graphService = new GraphService();
		graphService.readGraph(input);
	}

}
