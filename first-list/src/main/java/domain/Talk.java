package domain;

/**
 * Classe que representa os Talks. Os Talks nada mais s�o do que as
 * apresenta��es. Talks s�o representador por <em>name</em>, <em>length</em> e
 * <em>weight</em>.
 * <p>
 * Seja <em>length</em> a dura��o do Talk expressa em minutos.
 * <p>
 * Seja <em>weight</em> o peso to Talk.
 * <p>
 * O peso � definido como o impacto que sua dura��o tem sobre uma sess�o. Por
 * exemplo: Se uma sess�o tem dura��o m�xima de 120 minutos e o <em>length</em>
 * do Talk � definido como 60 minutos, ent�o o <em>weight</em> � 50, pois
 * equivale a 50% do tempo total do evento.
 * 
 * @author Pedro
 *
 */
public class Talk {

	private String name;
	private Integer length;
	private double weight;

	public Talk(String name, Integer length) {
		super();
		this.name = name;
		this.length = length;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((length == null) ? 0 : length.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Talk other = (Talk) obj;
		if (length == null) {
			if (other.length != null)
				return false;
		} else if (!length.equals(other.length))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Talk [name=" + name + ", length=" + length + ", weight=" + weight + "]";
	}

}
