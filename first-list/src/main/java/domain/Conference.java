package domain;

import java.util.ArrayList;
import java.util.List;

public class Conference {

	private String id;
	private List<Track> tracks;

	
	public Conference() {
		this.tracks = new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Track> getTracks() {
		return tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	@Override
	public String toString() {
		return "Conference [id=" + id + ", tracks=" + tracks + "]";
	}
	
	

}
