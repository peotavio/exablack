package domain;

public enum PeriodOfTime {
	MORNING(9, 12), AFTERNOON(13, 17);
	private final int start; // in minutes
	private final int end; // in minutes

	PeriodOfTime(int start, int end) {
		this.start = start;
		this.end = end;
	}

	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

}
