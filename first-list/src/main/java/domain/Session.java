package domain;

import java.util.List;

public class Session {

	private String id;
	private List<Talk> talks;
	public PeriodOfTime periodOfTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Talk> getTalks() {
		return talks;
	}

	public void setTalks(List<Talk> talks) {
		this.talks = talks;
	}

	public PeriodOfTime getPeriodOfTime() {
		return periodOfTime;
	}

	public void setPeriodOfTime(PeriodOfTime periodOfTime) {
		this.periodOfTime = periodOfTime;
	}

}
