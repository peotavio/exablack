# README #

Projeto MAVEN construído para o exame admissional EXABLACK

* Todo o conteúdo está em classes de testes
* Arquitetura em serviços
* Necessário Java 8

### Como rodar os testes? ###

* Trains

mvn -Dtest=GraphServiceTest test

mvn -Dtest=GraphServiceTest#testShortestPathGreedyImpl test

* Conference

mvn -Dtest=GraphServiceTest test

mvn -Dtest=GraphServiceTest#testShortestPathGreedyImpl test

### Projeto WebApp com Spark ###

* Migrate db com Carbon-Five
mvn clean db-migration:migrate install -DskipTests

* Compile, package and run
mvn compile package exec:java -Dexec.mainClass="App"

* link para testar a rota
http://localhost:4567/edge